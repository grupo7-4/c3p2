function agregarAlumno() {
    const cMatricula = document.getElementById('txtMatricula').value;
    const cNombre = document.getElementById('txtNombre').value;
    const cDomicilio = document.getElementById('txtDomicilio').value;
    const cSexo = document.getElementById('txtSexo').value;
    const cEspecialidad = document.getElementById('txtEspecialidad').value;
    if(cMatricula){
        const alumno = {
            matricula : cMatricula,
            nombre : cNombre,
            domicilio : cDomicilio, 
            sexo : cSexo,
            especialidad : cEspecialidad
        }
        axios.post('/insertar', alumno)
        .then((response) => {
            console.log(response.data);
            const res = document.getElementById('tabla');
            res.innerHTML = "<tr><th>ID</th><th>Matricula</th><th>Nombre</th><th>Domicilio</th><th>Sexo</th><th>Especialidad</th></tr>"
            mostrarDatos();
        })
        .catch((error) => {
            console.log(error);
            alert('Error al agregar el alumno');
        });
    }
}


function mostrarDatos(){   
    axios.get('/')
    .then((res)=>{
    mostrar(res.data);
    })
    .catch((error)=>{
        console.log("Surgio un error" + error);
    })
    function mostrar(data){
        const res = document.getElementById('tabla');

            for(item of data){
                    res.innerHTML += '<tr> <td>' + item.id + '</td> <td>' + item.matricula + '</td> <td>' + item.nombre + '</td><td>' + item.domicilio + 
                    '</td> <td>' + item.sexo + '</td> <td>' + item.especialidad + '</td> </tr>';
            }
    }
}

function buscar(){
    const cMatricula = document.getElementById('txtMatricula').value;
    if(cMatricula){
        axios.get('/buscar', {params: {matricula: cMatricula}})
        .then((res)=>{
            mostrar(res.data);
        })
        .catch((error)=>{
            console.log(error);
            alert('No se encuentra la matricula');
        })
        function mostrar(data){
            const res = document.getElementById('tabla');
            res.innerHTML = "<tr><th>ID</th><th>Matricula</th><th>Nombre</th><th>Domicilio</th><th>Sexo</th><th>Especialidad</th></tr>"

                for(item of data){
                    res.innerHTML += '<tr> <td>' + item.id + '</td> <td>' + item.matricula + '</td> <td>' + item.nombre + '</td><td>' + item.domicilio + 
                    '</td> <td>' + item.sexo + '</td> <td>' + item.especialidad + '</td> </tr>';

                    document.getElementById('txtMatricula').value = item.matricula;
                    document.getElementById('txtNombre').value = item.nombre;
                    document.getElementById('txtDomicilio').value = item.domicilio;
                    document.getElementById('txtSexo').value = item.sexo;
                    document.getElementById('txtEspecialidad').value = item.especialidad;
                }
        }
    }
}

function eliminar(){
const cMatricula = document.getElementById('txtMatricula').value;
if(cMatricula){
    axios.delete('/borrar', {params: {matricula:cMatricula}})
    .then((response) => {
        console.log(response.data);
        const res = document.getElementById('tabla');
        res.innerHTML = "<tr><th>ID</th><th>Matricula</th><th>Nombre</th><th>Domicilio</th><th>Sexo</th><th>Especialidad</th></tr>"
        mostrarDatos();
    })
    .catch((error) => {
        console.log(error);
        alert('Error al eliminar el alumno');
    });
}

}

function modificar(){
    const cMatricula = document.getElementById('txtMatricula').value;
    const cNombre = document.getElementById('txtNombre').value;
    const cDomicilio = document.getElementById('txtDomicilio').value;
    const cSexo = document.getElementById('txtSexo').value;
    const cEspecialidad = document.getElementById('txtEspecialidad').value;
if(cMatricula){
    const alumno = {
        nombre : cNombre,
        domicilio : cDomicilio, 
        sexo : cSexo,
        especialidad : cEspecialidad,
        matricula : cMatricula
    }
    axios.put('/actualizar', alumno)
    .then((response) => {
        console.log(response.data);
        const res = document.getElementById('tabla');
        res.innerHTML = "<tr><th>ID</th><th>Matricula</th><th>Nombre</th><th>Domicilio</th><th>Sexo</th><th>Especialidad</th></tr>"
        mostrarDatos();
        document.getElementById('txtMatricula').value = "";
        document.getElementById('txtNombre').value = "";
        document.getElementById('txtDomicilio').value = "";
        document.getElementById('txtSexo').value = "M";
        document.getElementById('txtEspecialidad').value = "";
    })
    .catch((error) => {
        console.log(error);
        alert('Error al actualizar el alumno');
    });
}
}

